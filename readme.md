# AWS DevOps IaC Assessment
 
 
The script below is an AWS CloudFormation yaml template that you can use to create the requested infrastructure in an AWS account. Feel free to use any AWS account for it. The template will build a simple network infrastructure where we have a load balancer with autoscaling group, a single server and a Redis database. You can freely customize the AMI instances to your work environment (Linux / Windows). 
 
The following two modifications need to be done on the infrastructure:
 
 
1. Find errors or misconfigured things in the infrastructure, guided by the best practice standards. Describe the found errors and correct them in the script itself.
 
2. Upgrade the script with the RDP option to the server and install the IIS component on the server using IaC practice (if you come from the Linux world, feel free to create the option with SSH connection and Nginx base installation). 
 
 
Please send us back the revised and updated template as a solution to the task.

Yaml template can be downloaded here: https://drive.google.com/file/d/16d_BDH6Q5Kz5I1xI44lwXIQ8mIr42YC5/view?usp=sharing


Source: https://docs.google.com/document/d/1A6v1_yolz7EMUWDAUQ3jBkVZdBmbkc0AdvzlejMJPtU/edit


# Topology

This is the desired state I wanted to achieve by reading the original template definitions:

![Schema](docs/res/schema.png)



# The stack

The original assigment is here:

[AWS.yaml - Original](cloudformation/original/AWS .yaml)

The rewritten template is here:

[AWS.yml - Rewritten](cloudformation/rewrite/AWS.yml)

I have rewritten, tested and modified the template to accomodate both windows and linux machines:

`aws cloudformation create-stack  --template-body file://rewrite/AWS.yml  --stack-name example --capabilities CAPABILITY_IAM --parameters ParameterKey=Webserver,ParameterValue=windows`

`aws cloudformation create-stack  --template-body file://rewrite/AWS.yml  --stack-name example --capabilities CAPABILITY_IAM --parameters ParameterKey=Webserver,ParameterValue=linux`

## Working deployment PoC:

I tried to screenshot both the LB url and server headers depending on type of server. 

### Windows:

![Poc-win](docs/res/poc-windows.png)

### Linux:

![Poc-win](docs/res/poc-linux.png)


# Terraform example

I have also included a (working) example of an autoscaling webserver group backed by a MySQL cluster (routed via HAProxy).
It is pretty similar to the above example, except it can loop over AZ's to create subnets in each and has fully configurable number of instances per AZ.

It depends somewhat on customized/prebuilt AMI's that I build via Ansible.
