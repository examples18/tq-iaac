variable webserver-ami {

    default = "ami-07e76acd5da0419a4"
}

variable mysql-ami {

    default= ""
}

variable lamp-ami {

    default = "ami-0f90065a1ea1e2b61"
}


variable "proxy-ami" {

    default = ""
}

variable "zone" {

    default = "production.com"
}

variable "fqdn-prod" {

    default = "production.com"
}

variable "fqdn-dev" {
    
    default = "dev.production.com"
}

variable key_name {

    description = "SSH access key"
    default = "aws-key-fb1"
}

  
variable db_nodes_per_sub {

    default = 1
}