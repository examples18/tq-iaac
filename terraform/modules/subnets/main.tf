resource "aws_subnet" "main" {

    count = length(data.aws_availability_zones.available.names)
    vpc_id     = var.vpc.id    

    #carve it up into /24 blocks
    cidr_block = cidrsubnet(var.vpc.cidr_block, 8, count.index+1+var.cidr_offset)

    tags = { Name = "${var.subnet_name}-${element(data.aws_availability_zones.available.names, count.index)}" }
    availability_zone = element(data.aws_availability_zones.available.names, count.index)
    map_public_ip_on_launch = var.map_public
}


resource "aws_route_table_association" "gw1" {
  count = length(aws_subnet.main.*)
  subnet_id      = aws_subnet.main[count.index].id
  route_table_id = var.vpc_route_table.id
}