variable vpc {

    description = "Referenced by running vpc module to create vpc"
}

variable cidr_offset {

    description = "Offset to begin creating /24 subnets in /16 main in case some are already in use."
    default =   0
}


variable vpc_route_table {

    description = "Referenced by running vpc module to create vpc route table"  
}


variable subnet_name {
    
    description = "Name of the subnet array"
    default = "Main"

}


variable map_public {
    description = "Map public IP's on subnet assignment"
    default = true

}

