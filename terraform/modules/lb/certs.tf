resource "aws_route53_record" "record" {
  
  zone_id = var.zone.zone_id
  name  = var.fqdn
  type = "A"

  alias {
    name                   = aws_lb.lb.dns_name
    zone_id                = aws_lb.lb.zone_id
    evaluate_target_health = true
  }
  
}


resource "aws_acm_certificate" "cert" {
  domain_name       = var.fqdn
  validation_method = "DNS"  

}

resource "aws_route53_record" "validation-record" {

 for_each = {
     for dvo in aws_acm_certificate.cert.domain_validation_options: dvo.domain_name  => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type     
     }
 }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = var.zone.zone_id

}


resource "aws_acm_certificate_validation" "validation" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.validation-record : record.fqdn]
}
