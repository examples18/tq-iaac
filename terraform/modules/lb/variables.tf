variable vpc {

    description =  "Refrenced by invoking a vpc module."
}

variable subnets {

    description =  "All subnets created by vpc - refrenced by invoking a vpc module."

}

variable "zone" {

    description = "Referenced from zone module"
  
}

variable "as" {
 description = "Austoscaling group, referenced from invoking other modules"
   
}

variable "sg" {
  
}

variable "fqdn" {
  description = "Main LB FQDN"

}

variable "health_check_path" {

  description = "Path to check for OK 200 on backend"
  default = "/"

}

variable "protect" {
  description  = "Protect from deletion. Production only. Wont be able to delete via terraform"
  default = "false"

}