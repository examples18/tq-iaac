resource "aws_lb" "lb" {

  name               = "lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.sg.*.id
            
  subnets            = var.subnets.*.id

  enable_deletion_protection = var.protect
}


resource "aws_lb_target_group" "tg" {
  name     = "tg-web"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc.id

  health_check  {
    enabled = true
    path  = var.health_check_path
  }
}


#THIS IS FOR AUTOSCALING
resource "aws_autoscaling_attachment" "lb-att" {

  autoscaling_group_name = var.as.id
  alb_target_group_arn   = aws_lb_target_group.tg.arn
}

 


resource "aws_lb_listener" "lb-web-listen" {

  load_balancer_arn = aws_lb.lb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn = aws_acm_certificate_validation.validation.certificate_arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
}
