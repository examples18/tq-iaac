data "template_file" "mount" {


    template =  file("./templates/mount.tpl")
    vars = {

        target = var.storage.dns_name
    }
    
}
