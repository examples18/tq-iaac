resource "aws_instance" "dev_lamp" {
   
    
    ami      = var.image
    instance_type = var.instance_type
    key_name  =   var.key_name
    subnet_id = var.subnets[0].id 
    private_ip = var.private_ip
    vpc_security_group_ids = [
        aws_security_group.allow_all_internal.id,
        aws_security_group.allow_ssh.id,
        aws_security_group.allow_outbound_all.id,
        aws_security_group.allow_https.id
    ]

    user_data = data.template_file.mount.rendered  
}

#Static IP reservation.
resource "aws_eip" "dev_ip" {
  instance = aws_instance.dev_lamp.id
  vpc      = true
  associate_with_private_ip = var.private_ip
}

