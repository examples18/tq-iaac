variable vpc {

    description =  "Refrenced by invoking a vpc module."
}

variable subnets {

    description =  "All subnets created by vpc - refrenced by invoking a vpc module."

}

variable image {

    description = "AMI ID to spin up"
    default = ""
}

variable storage {

    description = "Webroot EFS persistent storage"
}


variable instance_type {

    description = "what EC2 instance type to run"
    default = "t2.medium"
}


variable private_ip {

    description = "What private IP to assign to the machine."
    default = "10.10.10.100"    
}

variable fqdn {

    description = "I dont really need this..."

}

variable key_name {

    description = "SSH auth key name in EC2"
}

variable ssh_from {

    description = "where to allow SSH from"
    default = [ "0.0.0.0/0" ]

}