
resource "aws_security_group" "allow_all_internal" {
    
    name    =   "${var.fqdn} - allow_all_internal"
    description = "Allow access from all"
    vpc_id  =   var.vpc.id

    ingress {

            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = [ var.vpc.cidr_block ]
    }

}

resource "aws_security_group" "allow_ssh" {
    
    name    =   "${var.fqdn} -allow_ssh"
    description = "Allow SSH access"
    vpc_id  =   var.vpc.id

    ingress {

            from_port = 22
            to_port = 22
            protocol = "tcp"
            cidr_blocks = var.ssh_from
                }
}

resource "aws_security_group" "allow_outbound_all" {
    
    name    =   "${var.fqdn} -allow_outbound_all"
    description = "Allow all outbound traffic"
    vpc_id  =   var.vpc.id

    egress {

        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

}



resource "aws_security_group" "allow_https" {
    
    name    =   "${var.fqdn} -allow_http_only"
    description = "Allow HTTP access from all"
    vpc_id  =   var.vpc.id

    ingress {

            from_port = 80
            to_port = 80
            protocol = "tcp"
            cidr_blocks = [ "0.0.0.0/0"  ]
    }

    ingress {

            from_port = 443
            to_port = 443
            protocol = "tcp"
            cidr_blocks = [ "0.0.0.0/0"  ]
    }
      
}