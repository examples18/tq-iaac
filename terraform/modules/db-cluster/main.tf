resource "aws_instance" "instance" {
    
    count = length(var.subnets)*var.nodes_per_sub
    
    ami      = var.image
    instance_type = var.instance_type

    subnet_id = var.subnets[floor(count.index/var.nodes_per_sub)].id 
    private_ip = cidrhost (var.subnets[ floor(count.index/var.nodes_per_sub) ].cidr_block, var.host_offset+(count.index%var.nodes_per_sub))
    vpc_security_group_ids = [
        aws_security_group.allow_all_internal.id,
        aws_security_group.allow_ssh_netcom.id,
        aws_security_group.allow_outbound_all.id
    ]
    #user_data = var.user_data
    user_data = data.template_file.mycnf[count.index].rendered
    tags = { Name = "mysql-${count.index}" }
}