variable vpc {

    description =  "Refrenced by invoking a vpc module."
}

variable subnets {

    description =  "All subnets created by vpc - refrenced by invoking a vpc module."

}

variable cluster_name {

    description = "Cluster name. Must be unique"

}


variable ssh_from {

    description = "where to allow SSH from"
    default = [ "195.29.90.2/32" ]

}

variable image {

    description = "AMI ID to spin up"
    default = "ami-09817c02c0eb66a7b"
}

variable instance_type {

    description = "what EC2 instance type to run"
    default = "t2.micro"
}

variable number {

    description = "Number of instances to spin up"
    default = 5
}



variable host_offset {
    
    description = "First host in each subnet"
    default = 10

}

variable nodes_per_sub {

    description = "How many hosts to deploy per subnet"
    default = 1
}


variable sst_pass {

    description = "SST pass for replication"
    default = "sstpass"
}