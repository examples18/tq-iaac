locals {

    cluster_ips = [

        for ipi in range(0,length(var.subnets)*var.nodes_per_sub): 
            cidrhost (var.subnets[ floor(ipi/var.nodes_per_sub) ].cidr_block, var.host_offset+(ipi%var.nodes_per_sub))
    ]

}

data "template_file" "mycnf" {
    count = length(var.subnets)*var.nodes_per_sub
    template =  file("./templates/my.cnf.tpl")
    vars = {
        cluster_ips = join(",", local.cluster_ips ) 
        cluster_name = var.cluster_name
        node = "mysql-${count.index+1}"

        ip = local.cluster_ips[count.index]
        buffer = "500M"
        donor = "mysql-1"
        sst_pass = var.sst_pass
        sleep = "5m"
    }
    
}


