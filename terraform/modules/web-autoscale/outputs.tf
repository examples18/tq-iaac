output "as" {

    value = aws_autoscaling_group.as-web
}

output "sg" {

    value = [ aws_security_group.allow_all_internal, aws_security_group.allow_outbound_all, aws_security_group.allow_https ]
}