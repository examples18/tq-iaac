data "template_file" "mount" {

    //count = length(aws_subnet.autosub.*)
    template =  file("./templates/mount.tpl")
    vars = {
       #mount_ip = aws_efs_mount_target.webserver-mount-autosub[count.index].ip_address
        #target = aws_efs_file_system.webroot.dns_name
        target = var.storage.dns_name
    }
    
}
