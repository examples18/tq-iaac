variable vpc {

    description =  "Refrenced by invoking a vpc module."
}

variable subnets {

    description =  "All subnets created by vpc - refrenced by invoking a vpc module."

}

variable storage {

    description = "Webroot EFS persistent storage"
}

variable ssh_from {

    description = "where to allow SSH from"
    default = [ "195.29.90.2/32" ]

}

variable "image" {

    description = "AMI ID to spin up"
    default = "ami-09817c02c0eb66a7b"
}

variable instance_type {

    description = "what EC2 instance type to run"
    default = "t2.micro"
}

variable min_size {
    description = "Minimal size of autoscaler group"
    default = 1
}

variable max_size {
    description = "Maximal size of autoscaler group"
    default = 3
}

variable desired_size {
    description = "Desired size of autoscaler group"
    default = 3
}

variable fqdn {

    description = "I dont really need this..."

}

variable key_name {

    description = "SSH auth key name in EC2"
}