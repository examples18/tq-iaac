

resource "aws_launch_configuration" "lc-web" {
  name_prefix          = "lc-web"
  image_id      = var.image
  instance_type = var.instance_type
  key_name =  var.key_name
  associate_public_ip_address = true
  security_groups = [ 
      aws_security_group.allow_ssh_netcom.id,
      aws_security_group.allow_all_internal.id,
      #aws_security_group.allow_https.id,
      aws_security_group.allow_outbound_all.id
  ]
  #user_data = "mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-abb2f8f3.efs.eu-central-1.amazonaws.com:/ /opt/nginx/htdocs"
  #user_data = "sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${aws_efs_file_system.webroot.dns_name}:/ /opt/nginx/htdocs"
  user_data = data.template_file.mount.rendered
}

resource "aws_autoscaling_group" "as-web" {
  name_prefix                 = "as-web"
  launch_configuration = aws_launch_configuration.lc-web.name
  min_size             = var.min_size
  desired_capacity =    var.desired_size
  max_size             = var.max_size
  vpc_zone_identifier = var.subnets.*.id

  lifecycle {
    create_before_destroy = true
  }
}
