


resource "aws_efs_mount_target" "webmount" {

  count = length(var.subnets.*)
  file_system_id = var.storage.id
  subnet_id      = var.subnets[count.index].id
  security_groups = [       
      aws_security_group.allow_all_internal.id,      
      aws_security_group.allow_outbound_all.id
  ]
}

