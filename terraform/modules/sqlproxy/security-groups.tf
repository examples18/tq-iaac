
resource "aws_security_group" "allow_all_internal" {
    
    name    =   "HAPROXY - allow_all_internal"
    description = "Allow access from all"
    vpc_id  =   var.vpc.id

    ingress {

            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = [ var.vpc.cidr_block ]
    }

}

resource "aws_security_group" "allow_ssh" {
    
    name    =   "HAPROXY -allow_ssh"
    description = "Allow SSH access"
    vpc_id  =   var.vpc.id

    ingress {

            from_port = 22
            to_port = 22
            protocol = "tcp"
            cidr_blocks = var.ssh_from
                }
}

resource "aws_security_group" "allow_outbound_all" {
    
    name    =   "HAPROXY -allow_outbound_all"
    description = "Allow all outbound traffic"
    vpc_id  =   var.vpc.id

    egress {

        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

}



