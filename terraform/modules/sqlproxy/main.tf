resource "aws_instance" "proxy" {
   
    
    ami      = var.image
    instance_type = var.instance_type
    key_name  =   var.key_name
    subnet_id = var.subnets[0].id 
    private_ip = var.private_ip
    vpc_security_group_ids = [
        aws_security_group.allow_all_internal.id,
        aws_security_group.allow_ssh.id,
        aws_security_group.allow_outbound_all.id        
    ]

    user_data = data.template_file.haproxy.rendered 
}