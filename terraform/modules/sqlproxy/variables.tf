variable vpc {

    description =  "Refrenced by invoking a vpc module."
}

variable subnets {

    description =  "All subnets created by vpc - refrenced by invoking a vpc module."

}

variable image {

    description = "AMI ID to spin up"
    default = ""
}



variable instance_type {

    description = "what EC2 instance type to run"
    default = "t2.small"
}


variable private_ip {

    description = "What private IP to assign to the machine."
    default = "10.10.10.110"    
}

variable key_name {

    description = "SSH auth key name in EC2"
}

variable ssh_from {

    description = "where to allow SSH from"
    default = [ "0.0.0.0/0" ]

}

variable nodes_per_sub {

    description = "How many DB hosts are deployed per subnet"
    default = 1
}

variable host_offset {
    
    description = "First host in each subnet"
    default = 10

}