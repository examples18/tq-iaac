locals {    
        
    sql_nodes = [

        for ipi in range(0,length(var.subnets)*var.nodes_per_sub): 
            "server mysql-${ipi+1} ${cidrhost (var.subnets[ floor(ipi/var.nodes_per_sub) ].cidr_block, var.host_offset+(ipi%var.nodes_per_sub))}:3306 maxconn 1200 check port 9200 inter 12000 rise 3 fall 3  weight 20"
    ]

}

data "template_file" "haproxy" {
     
    template =  file("./templates/haproxy.cfg.tpl")
    vars = {

        sql_nodes = join("\n\t", local.sql_nodes ) 
        stats_user = "bist"
        stats_pass = "njenjenje"
        
    }
    
}


