terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/666/terraform/state/production" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/666/terraform/state/production/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/666/terraform/state/production/lock" \
    -backend-config="username=" \
    -backend-config="password=" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"