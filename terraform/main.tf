### GENEREATE NETWORK DEFS

module "main-vpc" {
  source = "./modules/vpc" 
  cidr = "10.10.0.0/16"
}

#Will generate 10.10.10.x  - 10.10.12.x subnets
module "web-subnets" {
  source = "./modules/subnets"    
  cidr_offset = 10
  subnet_name = "Webservers"
  map_public = true
  vpc = module.main-vpc.vpc
  vpc_route_table = module.main-vpc.route_table

}

#Will generate 10.10.20.x  - 10.10.22.x subnets
module "db-subnets" {
  source = "./modules/subnets"  

  cidr_offset = 20
  map_public = true
  subnet_name = "DB"
  vpc = module.main-vpc.vpc
  vpc_route_table = module.main-vpc.route_table
}
#### END GENERATE NETWORK DEFS

#### GENERATE EFS STORAGE FOR PERSISTENCE

resource "aws_efs_file_system" "webroot" {
   
}

resource "aws_efs_file_system" "backup" {
   
}


#### END GENERATE EFS STORAGE FOR PERSISTENCE

module "dev-server" {
  
  source = "./modules/web"
  vpc = module.main-vpc.vpc
  private_ip = "10.10.10.100"
  subnets = module.web-subnets.subnets
  key_name = var.key_name
  image = var.lamp-ami
  fqdn = var.fqdn-dev
  storage = aws_efs_file_system.webroot
  ssh_from = [ "0.0.0.0/0" ]
}

module "proxy" {
  
  source = "./modules/sqlproxy"
  vpc = module.main-vpc.vpc
  private_ip = "10.10.10.200"
  subnets = module.web-subnets.subnets
  nodes_per_sub = var.db_nodes_per_sub
  key_name = var.key_name
  image = var.lamp-ami
  ssh_from = [ "0.0.0.0/0" ]  
}



module "webservers" {
  
  source = "./modules/web-autoscale"  
  
  vpc = module.main-vpc.vpc
  subnets = module.web-subnets.subnets
  storage = aws_efs_file_system.webroot

  image = var.webserver-ami
  fqdn  = var.fqdn-prod
  min_size  = 1
  max_size  = 3
  desired_size = 2
  key_name = var.key_name
}


module "zone" {
  
  source = "./modules/zone"
  zone = var.zone

}

module "web-lb" {

  source = "./modules/lb"
  vpc = module.main-vpc.vpc
  subnets = module.web-subnets.subnets
  as = module.webservers.as
  zone = module.zone.zone  
  sg = module.webservers.sg

  protect = false
  fqdn = var.fqdn-prod
  health_check_path = "/phpinfo.php"
}


module "db-cluster" {


  vpc = module.main-vpc.vpc
  subnets = module.db-subnets.subnets

  source = "./modules/db-cluster"
  instance_type = "t2.micro"
  image = "ami-0f6741a81836db2cb"
  
  cluster_name = "cluster007"

  nodes_per_sub = var.db_nodes_per_sub 
  sst_pass = "replicirajmenjezno"

}
