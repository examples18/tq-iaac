#!/bin/bash

cat << EOF > /etc/my.cnf
[xtrabackup]
socket=/tmp/mysql.sock


[mysqld]
basedir= /opt/mysql
datadir=/opt/mysql/data
log-error = /var/log/mysqld.log

##############CLUSTER CONFIG

#per cluster config - same on all nodes
wsrep_cluster_name=${cluster_name}
wsrep_cluster_address=gcomm://${cluster_ips}
#wsrep_cluster_address=gcomm://
wsrep_sst_auth=sstuser:${sst_pass}

#per node config
wsrep_node_name=${node}
wsrep_node_address=${ ip }

#wsrep_sst_donor=${donor}

## no touchy.
wsrep_sst_method=xtrabackup-v2
wsrep_provider=/opt/mysql/lib/libgalera_smm.so
pxc_strict_mode=PERMISSIVE
binlog_format=ROW
default_storage_engine=InnoDB
innodb_autoinc_lock_mode=2
##################END CLUSTER CONFIG


sql_mode=''

default_storage_engine=InnoDB
max_connections = 700
skip_name_resolve

innodb_open_files=4000
innodb_file_per_table=1
innodb_data_home_dir = /opt/mysql/data
innodb_log_group_home_dir =  /opt/mysql/data
innodb_buffer_pool_size = ${buffer}
innodb_log_file_size = 512M
innodb_log_buffer_size = 4M
innodb_flush_log_at_trx_commit = 2
innodb_lock_wait_timeout = 50
innodb_flush_method=O_DIRECT
#innodb_thread_concurrency = 36
thread_pool_size=36
thread_handling=pool-of-threads
max_allowed_packet = 1M

query_cache_size = 0
tmp_table_size = 16M
sort_buffer_size = 2M
read_buffer_size = 2M
read_rnd_buffer_size=2M

myisam_sort_buffer_size = 128M

[mysql]
socket=/tmp/mysql.sock
EOF



node=${node}

if [ "$node" == "mysql-1" ]; then
    echo "This is the boostrap node." > /root/mysql.init.log
    systemctl start mysqld@bootstrap;
    sleep ${sleep};
    systemctl  stop mysqld@bootstrap;
else 
    echo "This is the slave node." > /root/mysql.init.log
fi    
systemctl start mysqld