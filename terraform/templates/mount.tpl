#!/bin/bash
retryCnt=15; 
waitTime=30;  
while true; do 
    sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${target}:/ /opt/nginx/htdocs; 
        if [ $? = 0 ] || [ $retryCnt -lt 1 ]; then 
            echo File system mounted successfully; break; 
        fi; 
    echo File system not available, retrying to mount.; 
    ((retryCnt--)); 
    sleep $waitTime; 
done;
