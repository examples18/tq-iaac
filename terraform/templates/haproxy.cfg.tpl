#!/bin/bash

cat << EOF > /etc/my.cnf
global
    log         127.0.0.1 local2

    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon

    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats

 
defaults
    log global
    mode http
    option tcplog
    option dontlognull
    retries 3
    option redispatch
    maxconn 2000
    contimeout 5000
    clitimeout 50000
    srvtimeout 50000


###### SQL READ
frontend pxc-front-read
    bind *:3306
    mode tcp
    default_backend pxc-back-read
    
backend pxc-back-read
    mode tcp    
    balance leastconn
    option httpchk

    ${sql_nodes}

###### SQL WRITE
frontend pxc-front-write
    bind *:3307
    mode tcp
    default_backend pxc-back-write

backend pxc-back-write
    mode tcp
    balance leastconn
    option httpchk
    option allbackups

    ${sql_nodes}

####### STATS
frontend stats-front
    bind *:80
    mode http
    default_backend stats-back


backend stats-back
    mode http
    balance roundrobin
    stats uri /haproxy/stats
    stats auth ${stats_user}:${stats_pass}
EOF